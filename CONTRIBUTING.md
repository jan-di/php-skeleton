# Contributing

## Setup Development Environment

1. Install [Lando](https://lando.dev/)
2. Run `lando start`

## Test/Fix Code

- `(lando) composer test`
- `(lando) composer fix`