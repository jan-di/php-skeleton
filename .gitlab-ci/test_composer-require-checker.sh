#/bin/sh
set -eux

phive --no-progress install --trust-gpg-keys D2CCAC42F6295E7D composer-require-checker
composer install --no-progress --no-scripts

tools/composer-require-checker check --config-file=$CI_PROJECT_DIR/composer-require-checker.json