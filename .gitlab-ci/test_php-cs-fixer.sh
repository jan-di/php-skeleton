#/bin/sh
set -eux

phive --no-progress install --trust-gpg-keys E82B2FB314E9906E php-cs-fixer

tools/php-cs-fixer fix --dry-run --allow-risky=yes