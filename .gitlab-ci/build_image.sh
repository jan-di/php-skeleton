#/bin/sh
set -eux

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH .
docker push --quiet $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH
