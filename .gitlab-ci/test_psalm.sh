#/bin/sh
set -eux

phive --no-progress install --trust-gpg-keys 8A03EA3B385DBAA1 psalm@4.1.1
composer install --no-progress --no-scripts

tools/psalm