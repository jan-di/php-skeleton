#!/bin/sh
set -eux

apk --no-cache add \
    gnupg \
    wget

wget --no-verbose -O phive.phar "https://phar.io/releases/phive.phar"
wget --no-verbose -O phive.phar.asc "https://phar.io/releases/phive.phar.asc"
gpg --keyserver hkps.pool.sks-keyservers.net --recv-keys 0x9D8A98B29B2D5D79
gpg --verify phive.phar.asc phive.phar
rm phive.phar.asc
chmod +x phive.phar
mv phive.phar /usr/local/bin/phive