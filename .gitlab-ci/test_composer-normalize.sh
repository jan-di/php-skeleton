#/bin/sh
set -eux

phive --no-progress install --trust-gpg-keys C00543248C87FB13 composer-normalize

tools/composer-normalize --dry-run