let mix = require('laravel-mix');

mix
    .sass('resources/sass/app.scss', 'public/assets')  
    .js('resources/js/app.js', 'public/assets').extract()
    .browserSync({
        proxy: 'http://appserver',
        socket: {
            domain: 'php-skeleton-bs.lndo.site',
            port: 80
        },
        open: false
    });