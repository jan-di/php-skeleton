<?php

declare(strict_types=1);

use App\Service\ResponseFactory;
use App\Service\Routing;
use Middlewares\RequestHandler;
use Psr\Container\ContainerInterface;
use function DI\autowire;
use function DI\get;
use function DI\string as str;

return [
    'app.root' => realpath(__DIR__.'/../..'),
    'app.env' => get('config:APP_ENV'),
    'app.title' => 'Title',

    'isprod' => get('app.env') == 'production',
    'isdev' => get('app.env') == 'development',

    'app.self.scheme' => get('config:APP_SELF_SCHEME'),
    'app.self.host' => get('config:APP_SELF_HOST'),
    'app.self.base' => get('config:APP_SELF_BASE'),

    'app.cache.dir' => str('{app.root}/var/cache'),
    'app.config.dir' => str('{app.root}/config'),
    'app.source.dir' => str('{app.root}/src'),

    Routing::class => autowire(),
    ResponseFactory::class => autowire(),

    RequestHandler::class => autowire()
        ->constructor(get(ContainerInterface::class)),
];
