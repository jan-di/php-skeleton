<?php

declare(strict_types=1);

use App\Application\Bootstrap\RouterFactory;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use function DI\autowire;
use function DI\factory;
use function DI\get;
use function DI\string as str;

return [
    'router.cache.dir' => str('{app.cache.dir}/router'),
    'router.controller.dir' => str('{app.source.dir}/Controller'),

    RouterFactory::class => autowire(),

    RouterInterface::class => get(Router::class),
    Router::class => factory([RouterFactory::class, 'createRouter'])
        ->parameter('appEnv', get('app.env'))
        ->parameter('cacheDir', get('router.cache.dir'))
        ->parameter('controllerDir', get('router.controller.dir')),
];
