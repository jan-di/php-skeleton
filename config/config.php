<?php

declare(strict_types=1);

use Jandi\Config\Entry\IntEntry;
use Jandi\Config\Entry\StringEntry;

return [
    (new StringEntry('APP_ENV'))->setAllowedValues(['development', 'production']),
    (new StringEntry('APP_SELF_SCHEME', 'http'))->setAllowedValues(['http', 'https']),
    (new StringEntry('APP_SELF_HOST', 'localhost')),
    (new StringEntry('APP_SELF_BASE', '')),
    (new StringEntry('DB_HOST', 'localhost')),
    (new IntEntry('DB_PORT', '3306'))->setLowerLimit(0)->setUpperLimit(65535),
    (new StringEntry('DB_NAME')),
    (new StringEntry('DB_USERNAME')),
    (new StringEntry('DB_PASSWORD')),
];
