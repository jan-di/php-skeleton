<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Service\ResponseFactory;
use App\Service\Routing;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\NoConfigurationException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class RoutingMiddleware implements MiddlewareInterface
{
    private Routing $routing;
    private ResponseFactory $responseFactory;

    public function __construct(
        Routing $routing,
        ResponseFactory $responseFactory
    ) {
        $this->routing = $routing;
        $this->responseFactory = $responseFactory;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            $routeInfo = $this->routing->matchRequest($request);
        } catch (ResourceNotFoundException $e) {
            return $this->responseFactory->createTwigHtmlResponse('error.html.twig', [
                'error' => [
                    'code' => 404,
                    'message' => 'not found',
                ],
            ], 404);
        } catch (MethodNotAllowedException $e) {
            return $this->responseFactory->createTwigHtmlResponse('error.html.twig', [
                'error' => [
                    'code' => 405,
                    'message' => 'method not allowed',
                ],
            ], 405);
        } catch (NoConfigurationException $e) {
            return $this->responseFactory->createTwigHtmlResponse('error.html.twig', [
                'error' => [
                    'code' => 500,
                    'message' => ' internal server error',
                ],
            ], 500);
        }

        foreach ($routeInfo as $key => $value) {
            $request = $request->withAttribute($key, $value);
        }

        return $handler->handle($request);
    }
}
