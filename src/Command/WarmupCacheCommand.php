<?php

declare(strict_types=1);

namespace App\Command;

use Psr\Container\ContainerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\Router;
use Twig\Environment;
use Twig\Error\SyntaxError;

class WarmupCacheCommand extends Command
{
    protected static $defaultName = 'cache:warmup';
    private ContainerInterface $container;
    private Environment $twig;

    public function __construct(
        ContainerInterface $container,
        Environment $twig
    ) {
        parent::__construct();

        $this->container = $container;
        $this->twig = $twig;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Warming up cache..');

        if ($this->container->get('config:APP_ENV') === 'production') {
            // Config and Container is already warmed up at this point.

            // Compile Routes
            $this->container->get(Router::class);

            // Compile Twig Templates
            $templateDir = (string) $this->container->get('twig.template.dir');
            $templateIterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($templateDir, RecursiveDirectoryIterator::SKIP_DOTS));
            foreach ($templateIterator as $file) {
                if ($templateIterator->getExtension() === 'twig') {
                    try {
                        $this->twig->render($templateIterator->getSubPathName());
                    } catch (SyntaxError $e) {
                    }
                }
            }

            $io->success('Cache warmed up successfully');
        } else {
            $io->note('Skipped cache-warmup (Server does not run in production environment)');
        }

        return Command::SUCCESS;
    }
}
