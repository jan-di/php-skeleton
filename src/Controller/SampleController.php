<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\ResponseFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Routing\Annotation\Route;

class SampleController
{
    private ResponseFactory $responseFactory;

    public function __construct(
        ResponseFactory $responseFactory
    ) {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @Route("/",name="index")
     */
    public function index(ServerRequestInterface $request): ResponseInterface
    {
        return $this->responseFactory->createTwigHtmlResponse('index.html.twig');
    }

    /**
     * @Route("/page1",name="page1")
     */
    public function page1(ServerRequestInterface $request): ResponseInterface
    {
        return $this->responseFactory->createTwigHtmlResponse('page1.html.twig');
    }

    /**
     * @Route("/page2",name="page2")
     */
    public function page2(ServerRequestInterface $request): ResponseInterface
    {
        return $this->responseFactory->createTwigHtmlResponse('page2.html.twig');
    }

    /**
     * @Route("/indexredirect",name="indexredirect")
     */
    public function redirectText(ServerRequestInterface $request): ResponseInterface
    {
        return $this->responseFactory->createRedirectResponse('index');
    }
}
