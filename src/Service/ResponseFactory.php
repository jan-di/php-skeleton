<?php

declare(strict_types=1);

namespace App\Service;

use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Psr\Http\Message\ResponseInterface;
use Twig\Environment;

class ResponseFactory
{
    private Routing $routing;
    private Environment $twig;

    public function __construct(
        Routing $routing,
        Environment $twig
    ) {
        $this->routing = $routing;
        $this->twig = $twig;
    }

    public function createTwigHtmlResponse(string $template, array $context = [], int $status = 200): ResponseInterface
    {
        $html = $this->twig->render($template, $context);

        return new HtmlResponse($html, $status);
    }

    public function createRedirectResponse(string $route, array $parameters = []): ResponseInterface
    {
        $url = $this->routing->generateUrl($route, $parameters);

        return new RedirectResponse($url);
    }

    public function createExternalRedirectResponse(string $url): ResponseInterface
    {
        return new RedirectResponse($url);
    }
}
