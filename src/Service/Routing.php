<?php

declare(strict_types=1);

namespace App\Service;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;

class Routing
{
    private Router $router;
    private RequestContext $absoluteContext;
    private HttpFoundationFactory $symfonyRequestFactory;

    public function __construct(
        Router $router,
        ContainerInterface $container,
        HttpFoundationFactory $symfonyRequestFactory
    ) {
        $this->router = $router;
        $this->symfonyRequestFactory = $symfonyRequestFactory;

        $this->absoluteContext = new RequestContext(
            $container->get('app.self.base'),
            'GET',
            $container->get('app.self.host'),
            $container->get('app.self.scheme')
        );
    }

    public function matchRequest(ServerRequestInterface $request): array
    {
        $symfonyRequest = $this->symfonyRequestFactory->createRequest($request);
        $this->router->getContext()->fromRequest($symfonyRequest);

        return $this->router->matchRequest($symfonyRequest);
    }

    public function generateUrl(string $route, array $parameters = []): string
    {
        return $this->router->generate($route, $parameters);
    }

    public function generateAbsoluteUrl(string $route, array $parameters = []): string
    {
        $currentContext = $this->router->getContext();

        $this->router->setContext($this->absoluteContext);
        $url = $this->router->generate($route, $parameters, Router::ABSOLUTE_URL);
        $this->router->setContext($currentContext);

        return $url;
    }
}
