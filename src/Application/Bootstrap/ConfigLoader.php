<?php

declare(strict_types=1);

namespace App\Application\Bootstrap;

use Dotenv\Dotenv;
use Jandi\Config\Config;
use Jandi\Config\ConfigBuilder;
use Jandi\Config\Dotenv\VlucasDotenvAdapter;

class ConfigLoader
{
    private ConfigBuilder $builder;

    public function __construct()
    {
        $definitions = require __DIR__.'/../../../config/config.php';
        $dotenv = Dotenv::createImmutable(__DIR__.'/../../..', '.env');

        $this->builder = new ConfigBuilder($definitions);
        $this->builder
            ->enableCaching(__DIR__.'/../../../var/cache/dotenv/config.php')
            ->enableDotEnv(new VlucasDotenvAdapter($dotenv));
    }

    public function loadConfig(): Config
    {
        $config = $this->builder->build();

        if (!$config->isCached() && $config->getValue('APP_ENV') === 'production') {
            $this->builder->dumpCache($config);
        }

        return $config;
    }
}
