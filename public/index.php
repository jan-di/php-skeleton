<?php

declare(strict_types=1);

use App\Application\WebApplication;

require __DIR__.'/../vendor/autoload.php';

$app = new WebApplication();
$app->execute();
