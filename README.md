# php-skeleton

My personal opinionated starting point for new PHP applications. Feel free to use.

## Content 

- PHP Backend Application
    - [PSR-15](https://www.php-fig.org/psr/psr-15/) compliant Middleware Stack:
      - [Symfony Router](https://symfony.com/doc/current/routing.html)
      - [Request Handler](https://github.com/middlewares/request-handler)
    - Console Entrypoint with [Symfony/Console](https://symfony.com/doc/current/components/console.html)
    - [Twig](https://twig.symfony.com/) Template Engine
- Some Frontend Tools
  - Compiled assets files via [Laravel Mix](https://laravel-mix.com/),
    including SASS compilation, PostCSS, javascript packing and asset minimization.
- Development environment:
  - [Lando](https://lando.dev/) configuration
  - Additional tools:
    - [php-cs-fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer)
    - [composer-normalize](https://github.com/ergebnis/composer-normalize)
    - [composer-require-checker](https://github.com/maglnet/ComposerRequireChecker)
    - [Psalm](https://psalm.dev/)
  - Custom php-cs-fixer configuration (Symfony/PSR-2 oriented)
- Buildscript to build a production ready distribution and a dockerized application.
- GitLab Pipeline including:
  - Tests with the aforementioned tools
  - GitLab [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
  - GitLab [License Scanning](https://docs.gitlab.com/ee/user/compliance/license_compliance)
  - GitLab [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/)
  - GitLab [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)